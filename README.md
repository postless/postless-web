# Postless

## Start locally (not so good ..)

Preferable use the docker-compose based setup, see next paragraph.

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create a dotenv file in .env
  * Start the Database by running `docker-compose up`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Use docker-compose (better ..)

This project ships with a ready to go `docker-compose` file. Simply do the following and you can skip all the local setup with asdf, postgres, etc.

### Setup

0. Create a dotenv file at `.env`. See `.env.template` for inspiration.
0. Run `docker-compose build`
0. Run `docker-compose run postless mix ecto.create` to create the DB.

### Run

0. Run `docker-compose up` 

### Migrations

To run migrations:

`docker-compose run postless mix ecto.migrate`

Similar for seeds:

`docker-compose run postless mix phil_columns.seed`

### Attach to a running container

To avoid permission conflicts run commands like `mix ecto.*` inside the container.

`docker exec -ti <container name> /bin/bash`

### Debugging

To run a debugging session with `pry` attach to the running postless node via remote shell. 

Attach to the running container with `docker exec -ti <container name> /bin/bash`.

Find the node name with `ps auxf | grep elixir`.

Attach to it with `iex --sname console --cookie monster --remsh <node ame>`


