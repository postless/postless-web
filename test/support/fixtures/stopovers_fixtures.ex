defmodule Postless.StopoversFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Postless.Stopovers` context.
  """

  @doc """
  Generate a stopover.
  """
  def stopover_fixture(attrs \\ %{}) do
    {:ok, stopover} =
      attrs
      |> Enum.into(%{
        address: "some address",
        status: :unpublished
      })
      |> Postless.Stopovers.create_stopover()

    stopover
  end

  @doc """
  Generate a stopover.
  """
  def stopover_fixture(attrs \\ %{}) do
    {:ok, stopover} =
      attrs
      |> Enum.into(%{})
      |> Postless.Stopovers.create_stopover()

    stopover
  end
end
