defmodule Postless.ShipmentsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Postless.Shipments` context.
  """

  @doc """
  Generate a shipment.
  """
  def shipment_fixture(attrs \\ %{}) do
    {:ok, shipment} =
      attrs
      |> Enum.into(%{
        address: "some address",
        comment: "some comment",
        is_deliverd: true,
        requested_delivery: ~N[2023-04-14 13:46:00]
      })
      |> Postless.Shipments.create_shipment()

    shipment
  end

  @doc """
  Generate a shipment.
  """
  def shipment_fixture(attrs \\ %{}) do
    {:ok, shipment} =
      attrs
      |> Enum.into(%{})
      |> Postless.Shipments.create_shipment()

    shipment
  end
end
