defmodule Postless.ShipmentsTest do
  use Postless.DataCase

  alias Postless.Shipments

  describe "shipments" do
    alias Postless.Shipments.Shipment

    import Postless.ShipmentsFixtures

    @invalid_attrs %{address: nil, comment: nil, is_deliverd: nil, requested_delivery: nil}

    test "list_shipments/0 returns all shipments" do
      shipment = shipment_fixture()
      assert Shipments.list_shipments() == [shipment]
    end

    test "get_shipment!/1 returns the shipment with given id" do
      shipment = shipment_fixture()
      assert Shipments.get_shipment!(shipment.id) == shipment
    end

    test "create_shipment/1 with valid data creates a shipment" do
      valid_attrs = %{
        address: "some address",
        comment: "some comment",
        is_deliverd: true,
        requested_delivery: ~N[2023-04-14 13:46:00]
      }

      assert {:ok, %Shipment{} = shipment} = Shipments.create_shipment(valid_attrs)
      assert shipment.address == "some address"
      assert shipment.comment == "some comment"
      assert shipment.is_deliverd == true
      assert shipment.requested_delivery == ~N[2023-04-14 13:46:00]
    end

    test "create_shipment/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Shipments.create_shipment(@invalid_attrs)
    end

    test "update_shipment/2 with valid data updates the shipment" do
      shipment = shipment_fixture()

      update_attrs = %{
        address: "some updated address",
        comment: "some updated comment",
        is_deliverd: false,
        requested_delivery: ~N[2023-04-15 13:46:00]
      }

      assert {:ok, %Shipment{} = shipment} = Shipments.update_shipment(shipment, update_attrs)
      assert shipment.address == "some updated address"
      assert shipment.comment == "some updated comment"
      assert shipment.is_deliverd == false
      assert shipment.requested_delivery == ~N[2023-04-15 13:46:00]
    end

    test "update_shipment/2 with invalid data returns error changeset" do
      shipment = shipment_fixture()
      assert {:error, %Ecto.Changeset{}} = Shipments.update_shipment(shipment, @invalid_attrs)
      assert shipment == Shipments.get_shipment!(shipment.id)
    end

    test "delete_shipment/1 deletes the shipment" do
      shipment = shipment_fixture()
      assert {:ok, %Shipment{}} = Shipments.delete_shipment(shipment)
      assert_raise Ecto.NoResultsError, fn -> Shipments.get_shipment!(shipment.id) end
    end

    test "change_shipment/1 returns a shipment changeset" do
      shipment = shipment_fixture()
      assert %Ecto.Changeset{} = Shipments.change_shipment(shipment)
    end
  end

  describe "shipments" do
    alias Postless.Shipments.Shipment

    import Postless.ShipmentsFixtures

    @invalid_attrs %{}

    test "list_shipments/0 returns all shipments" do
      shipment = shipment_fixture()
      assert Shipments.list_shipments() == [shipment]
    end

    test "get_shipment!/1 returns the shipment with given id" do
      shipment = shipment_fixture()
      assert Shipments.get_shipment!(shipment.id) == shipment
    end

    test "create_shipment/1 with valid data creates a shipment" do
      valid_attrs = %{}

      assert {:ok, %Shipment{} = shipment} = Shipments.create_shipment(valid_attrs)
    end

    test "create_shipment/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Shipments.create_shipment(@invalid_attrs)
    end

    test "update_shipment/2 with valid data updates the shipment" do
      shipment = shipment_fixture()
      update_attrs = %{}

      assert {:ok, %Shipment{} = shipment} = Shipments.update_shipment(shipment, update_attrs)
    end

    test "update_shipment/2 with invalid data returns error changeset" do
      shipment = shipment_fixture()
      assert {:error, %Ecto.Changeset{}} = Shipments.update_shipment(shipment, @invalid_attrs)
      assert shipment == Shipments.get_shipment!(shipment.id)
    end

    test "delete_shipment/1 deletes the shipment" do
      shipment = shipment_fixture()
      assert {:ok, %Shipment{}} = Shipments.delete_shipment(shipment)
      assert_raise Ecto.NoResultsError, fn -> Shipments.get_shipment!(shipment.id) end
    end

    test "change_shipment/1 returns a shipment changeset" do
      shipment = shipment_fixture()
      assert %Ecto.Changeset{} = Shipments.change_shipment(shipment)
    end
  end
end
