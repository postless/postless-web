defmodule Postless.StopoversTest do
  use Postless.DataCase

  alias Postless.Stopovers

  describe "stopovers" do
    alias Postless.Stopovers.Stopover

    import Postless.StopoversFixtures

    @invalid_attrs %{address: nil, status: nil}

    test "list_stopovers/0 returns all stopovers" do
      stopover = stopover_fixture()
      assert Stopovers.list_stopovers() == [stopover]
    end

    test "get_stopover!/1 returns the stopover with given id" do
      stopover = stopover_fixture()
      assert Stopovers.get_stopover!(stopover.id) == stopover
    end

    test "create_stopover/1 with valid data creates a stopover" do
      valid_attrs = %{address: "some address", status: :unpublished}

      assert {:ok, %Stopover{} = stopover} = Stopovers.create_stopover(valid_attrs)
      assert stopover.address == "some address"
      assert stopover.status == :unpublished
    end

    test "create_stopover/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Stopovers.create_stopover(@invalid_attrs)
    end

    test "update_stopover/2 with valid data updates the stopover" do
      stopover = stopover_fixture()
      update_attrs = %{address: "some updated address", status: :unpublished}

      assert {:ok, %Stopover{} = stopover} = Stopovers.update_stopover(stopover, update_attrs)
      assert stopover.address == "some updated address"
      assert stopover.status == :unpublished
    end

    test "update_stopover/2 with invalid data returns error changeset" do
      stopover = stopover_fixture()
      assert {:error, %Ecto.Changeset{}} = Stopovers.update_stopover(stopover, @invalid_attrs)
      assert stopover == Stopovers.get_stopover!(stopover.id)
    end

    test "delete_stopover/1 deletes the stopover" do
      stopover = stopover_fixture()
      assert {:ok, %Stopover{}} = Stopovers.delete_stopover(stopover)
      assert_raise Ecto.NoResultsError, fn -> Stopovers.get_stopover!(stopover.id) end
    end

    test "change_stopover/1 returns a stopover changeset" do
      stopover = stopover_fixture()
      assert %Ecto.Changeset{} = Stopovers.change_stopover(stopover)
    end
  end

  describe "stopovers" do
    alias Postless.Stopovers.Stopover

    import Postless.StopoversFixtures

    @invalid_attrs %{}

    test "list_stopovers/0 returns all stopovers" do
      stopover = stopover_fixture()
      assert Stopovers.list_stopovers() == [stopover]
    end

    test "get_stopover!/1 returns the stopover with given id" do
      stopover = stopover_fixture()
      assert Stopovers.get_stopover!(stopover.id) == stopover
    end

    test "create_stopover/1 with valid data creates a stopover" do
      valid_attrs = %{}

      assert {:ok, %Stopover{} = stopover} = Stopovers.create_stopover(valid_attrs)
    end

    test "create_stopover/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Stopovers.create_stopover(@invalid_attrs)
    end

    test "update_stopover/2 with valid data updates the stopover" do
      stopover = stopover_fixture()
      update_attrs = %{}

      assert {:ok, %Stopover{} = stopover} = Stopovers.update_stopover(stopover, update_attrs)
    end

    test "update_stopover/2 with invalid data returns error changeset" do
      stopover = stopover_fixture()
      assert {:error, %Ecto.Changeset{}} = Stopovers.update_stopover(stopover, @invalid_attrs)
      assert stopover == Stopovers.get_stopover!(stopover.id)
    end

    test "delete_stopover/1 deletes the stopover" do
      stopover = stopover_fixture()
      assert {:ok, %Stopover{}} = Stopovers.delete_stopover(stopover)
      assert_raise Ecto.NoResultsError, fn -> Stopovers.get_stopover!(stopover.id) end
    end

    test "change_stopover/1 returns a stopover changeset" do
      stopover = stopover_fixture()
      assert %Ecto.Changeset{} = Stopovers.change_stopover(stopover)
    end
  end
end
