defmodule PostlessWeb.ShipmentControllerTest do
  use PostlessWeb.ConnCase

  import Postless.ShipmentsFixtures

  @create_attrs %{
    address: "some address",
    comment: "some comment",
    is_deliverd: true,
    requested_delivery: ~N[2023-04-14 13:46:00]
  }
  @update_attrs %{
    address: "some updated address",
    comment: "some updated comment",
    is_deliverd: false,
    requested_delivery: ~N[2023-04-15 13:46:00]
  }
  @invalid_attrs %{address: nil, comment: nil, is_deliverd: nil, requested_delivery: nil}

  describe "index" do
    test "lists all shipments", %{conn: conn} do
      conn = get(conn, ~p"/shipments")
      assert html_response(conn, 200) =~ "Listing Shipments"
    end
  end

  describe "new shipment" do
    test "renders form", %{conn: conn} do
      conn = get(conn, ~p"/shipments/new")
      assert html_response(conn, 200) =~ "New Shipment"
    end
  end

  describe "create shipment" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, ~p"/shipments", shipment: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == ~p"/shipments/#{id}"

      conn = get(conn, ~p"/shipments/#{id}")
      assert html_response(conn, 200) =~ "Shipment #{id}"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, ~p"/shipments", shipment: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Shipment"
    end
  end

  describe "edit shipment" do
    setup [:create_shipment]

    test "renders form for editing chosen shipment", %{conn: conn, shipment: shipment} do
      conn = get(conn, ~p"/shipments/#{shipment}/edit")
      assert html_response(conn, 200) =~ "Edit Shipment"
    end
  end

  describe "update shipment" do
    setup [:create_shipment]

    test "redirects when data is valid", %{conn: conn, shipment: shipment} do
      conn = put(conn, ~p"/shipments/#{shipment}", shipment: @update_attrs)
      assert redirected_to(conn) == ~p"/shipments/#{shipment}"

      conn = get(conn, ~p"/shipments/#{shipment}")
      assert html_response(conn, 200) =~ "some updated address"
    end

    test "renders errors when data is invalid", %{conn: conn, shipment: shipment} do
      conn = put(conn, ~p"/shipments/#{shipment}", shipment: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Shipment"
    end
  end

  describe "delete shipment" do
    setup [:create_shipment]

    test "deletes chosen shipment", %{conn: conn, shipment: shipment} do
      conn = delete(conn, ~p"/shipments/#{shipment}")
      assert redirected_to(conn) == ~p"/shipments"

      assert_error_sent 404, fn ->
        get(conn, ~p"/shipments/#{shipment}")
      end
    end
  end

  defp create_shipment(_) do
    shipment = shipment_fixture()
    %{shipment: shipment}
  end
end
