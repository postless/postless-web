defmodule PostlessWeb.StopoverControllerTest do
  use PostlessWeb.ConnCase

  import Postless.StopoversFixtures

  @create_attrs %{address: "some address", status: :unpublished}
  @update_attrs %{address: "some updated address", status: :unpublished}
  @invalid_attrs %{address: nil, status: nil}

  describe "index" do
    test "lists all stopovers", %{conn: conn} do
      conn = get(conn, ~p"/stopovers")
      assert html_response(conn, 200) =~ "Listing Stopovers"
    end
  end

  describe "new stopover" do
    test "renders form", %{conn: conn} do
      conn = get(conn, ~p"/stopovers/new")
      assert html_response(conn, 200) =~ "New Stopover"
    end
  end

  describe "create stopover" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, ~p"/stopovers", stopover: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == ~p"/stopovers/#{id}"

      conn = get(conn, ~p"/stopovers/#{id}")
      assert html_response(conn, 200) =~ "Stopover #{id}"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, ~p"/stopovers", stopover: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Stopover"
    end
  end

  describe "edit stopover" do
    setup [:create_stopover]

    test "renders form for editing chosen stopover", %{conn: conn, stopover: stopover} do
      conn = get(conn, ~p"/stopovers/#{stopover}/edit")
      assert html_response(conn, 200) =~ "Edit Stopover"
    end
  end

  describe "update stopover" do
    setup [:create_stopover]

    test "redirects when data is valid", %{conn: conn, stopover: stopover} do
      conn = put(conn, ~p"/stopovers/#{stopover}", stopover: @update_attrs)
      assert redirected_to(conn) == ~p"/stopovers/#{stopover}"

      conn = get(conn, ~p"/stopovers/#{stopover}")
      assert html_response(conn, 200) =~ "some updated address"
    end

    test "renders errors when data is invalid", %{conn: conn, stopover: stopover} do
      conn = put(conn, ~p"/stopovers/#{stopover}", stopover: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Stopover"
    end
  end

  describe "delete stopover" do
    setup [:create_stopover]

    test "deletes chosen stopover", %{conn: conn, stopover: stopover} do
      conn = delete(conn, ~p"/stopovers/#{stopover}")
      assert redirected_to(conn) == ~p"/stopovers"

      assert_error_sent 404, fn ->
        get(conn, ~p"/stopovers/#{stopover}")
      end
    end
  end

  defp create_stopover(_) do
    stopover = stopover_fixture()
    %{stopover: stopover}
  end
end
