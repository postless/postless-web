defmodule Postless.Repo.Migrations.CreateShipments do
  use Ecto.Migration

  def change do
    create_if_not_exists table(:shipments, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :address, :string
      add :is_delivered, :boolean, default: false, null: false
      add :comment, :string
      add :requested_delivery, :naive_datetime

      add :type_id, references(:shipment_types, on_delete: :nothing, type: :binary_id),
        null: false

      add :sender_id, references(:users, on_delete: :nothing, type: :binary_id), null: false
      add :receiver_id, references(:users, on_delete: :nothing, type: :binary_id), null: true

      timestamps()
    end

    create_if_not_exists index(:shipments, [:type_id])
    create_if_not_exists index(:shipments, [:sender_id])
    create_if_not_exists index(:shipments, [:receiver_id])
  end
end
