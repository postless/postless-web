defmodule Postless.Repo.Migrations.CreateShipmentType do
  use Ecto.Migration

  def change do
    create_if_not_exists table(:shipment_types, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :type, :string
      add :points, :integer

      timestamps()
    end

    create_if_not_exists unique_index(:shipment_types, [:type])
  end
end
