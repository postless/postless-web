defmodule Postless.Repo.Migrations.CreateStopovers do
  use Ecto.Migration

  def change do
    create_if_not_exists table(:stopovers, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :status, :string
      add :address, :string, null: true
      add :shipment_id, references(:shipments, on_delete: :nothing, type: :binary_id)
      add :type_id, references(:stopover_types, on_delete: :nothing, type: :binary_id)
      add :user_id, references(:users, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create_if_not_exists index(:stopovers, [:shipment_id])
    create_if_not_exists index(:stopovers, [:type_id])
    create_if_not_exists index(:stopovers, [:user_id])
  end
end
