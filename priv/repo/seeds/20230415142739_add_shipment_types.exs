defmodule Postless.Repo.Seeds.AddShipmentTypes do
  use Postless.Seed

  alias Postless.Shipments.ShipmentType

  envs [:dev, :prod]

  def up(repo) do
    repo.insert(
      ShipmentType.changeset(
        %ShipmentType{},
        %{type: "card", points: 1}
      )
    )

    repo.insert(
      ShipmentType.changeset(
        %ShipmentType{},
        %{type: "letter", points: 1}
      )
    )

    repo.insert(
      ShipmentType.changeset(
        %ShipmentType{},
        %{type: "parcel", points: 2}
      )
    )

    repo.insert(
      ShipmentType.changeset(
        %ShipmentType{},
        %{type: "packet", points: 2}
      )
    )

    repo.insert(
      ShipmentType.changeset(
        %ShipmentType{},
        %{type: "bulky", points: 5}
      )
    )
  end
end
