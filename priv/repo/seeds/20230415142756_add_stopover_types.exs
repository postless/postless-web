defmodule Postless.Repo.Seeds.AddStopoverTypes do
  use Postless.Seed

  alias Postless.Stopovers.StopoverType

  envs [:dev, :prod]

  def up(repo) do
    Postless.Repo.insert(
      StopoverType.changeset(
        %StopoverType{},
        %{type: "start", points: 1}
      )
    )

    Postless.Repo.insert(
      StopoverType.changeset(
        %StopoverType{},
        %{type: "ongoing", points: 1}
      )
    )

    Postless.Repo.insert(
      StopoverType.changeset(
        %StopoverType{},
        %{type: "delivery", points: 2}
      )
    )
  end
end
