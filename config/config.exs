# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :postless,
  ecto_repos: [Postless.Repo]

# Configures the endpoint
config :postless, PostlessWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [
    formats: [html: PostlessWeb.ErrorHTML, json: PostlessWeb.ErrorJSON],
    layout: false
  ],
  pubsub_server: Postless.PubSub,
  live_view: [signing_salt: "su/P4S1L"]

# Configures the mailer
#
# By default it uses the "Local" adapter which stores the emails
# locally. You can see the emails in your browser, at "/dev/mailbox".
#
# For production it's recommended to configure a different adapter
# at the `config/runtime.exs`.
# config :postless, Postless.Mailer, adapter: Swoosh.Adapters.Local
config :postless, Postless.Mailer,
  adapter: Bamboo.SMTPAdapter,
  server: System.get_env("EMAIL_SMTP_SERVER"),
  port: System.get_env("EMAIL_SMTP_PORT", "587"),
  username: System.get_env("EMAIL_SMTP_USER"),
  password: System.get_env("EMAIL_SMTP_PASSWORD"),
  tls: :if_available,
  tls_verify: :verify_none,
  auth: :always,
  ssl: System.get_env("EMAIL_SMTP_SSL", "false") == "true",
  retries: System.get_env("EMAIL_SMTP_RETRIES", "1") |> Integer.parse()

config :postless, :generators,
  migration: true,
  binary_id: true,
  sample_binary_id: "11111111-1111-1111-1111-111111111111"

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.14.41",
  default: [
    args:
      ~w(js/app.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configure tailwind (the version is required)
config :tailwind,
  version: "3.1.8",
  default: [
    args: ~w(
      --config=tailwind.config.js
      --input=css/app.css
      --output=../priv/static/assets/app.css
    ),
    cd: Path.expand("../assets", __DIR__)
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Use German as default language
config :gettext, :default_locale, "de"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
