defmodule Postless.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      PostlessWeb.Telemetry,
      # Start the Ecto repository
      Postless.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: Postless.PubSub},
      # Start Finch
      {Finch, name: Postless.Finch},
      # Start the Endpoint (http/https)
      PostlessWeb.Endpoint
      # Start a worker by calling: Postless.Worker.start_link(arg)
      # {Postless.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Postless.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    PostlessWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
