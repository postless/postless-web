defmodule Postless.Email do
  @moduledoc """
  Module to send emails within postless.
  """

  import Bamboo.Email

  def test_mail do
    new_email(
      to: "boeseSchwingung@gmx.de",
      from: "boeseSchwingung@gmx.de",
      subject: "Postless Mailservice",
      text_body:
        "This is where your login code will be in the future. But kaputse and boeseSchwingung preferred to drink a coffee and a beer and relax instead of finishing building me ;)"
    )
  end
end
