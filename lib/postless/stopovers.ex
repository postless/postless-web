defmodule Postless.Stopovers do
  @moduledoc """
  The Stopovers context.
  """

  import Ecto.Query, warn: false
  alias Postless.Repo

  alias Postless.Stopovers.Stopover

  @doc """
  Returns the list of stopovers.

  ## Examples

      iex> list_stopovers()
      [%Stopover{}, ...]

  """
  def list_stopovers do
    Repo.all(Stopover)
  end

  @doc """
  Gets a single stopover.

  Raises if the Stopover does not exist.

  ## Examples

      iex> get_stopover!(123)
      %Stopover{}

  """
  def get_stopover!(id), do: Repo.get!(Stopover, id) |> Repo.preload(:user)

  @doc """
  Creates a stopover.

  ## Examples

      iex> create_stopover(%{field: value})
      {:ok, %Stopover{}}

      iex> create_stopover(%{field: bad_value})
      {:error, ...}

  """
  def create_stopover(attrs \\ %{}) do
    %Stopover{}
    |> Stopover.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a stopover.

  ## Examples

      iex> update_stopover(stopover, %{field: new_value})
      {:ok, %Stopover{}}

      iex> update_stopover(stopover, %{field: bad_value})
      {:error, ...}

  """
  def update_stopover(%Stopover{} = stopover, attrs) do
    stopover
    |> Stopover.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Stopover.

  ## Examples

      iex> delete_stopover(stopover)
      {:ok, %Stopover{}}

      iex> delete_stopover(stopover)
      {:error, ...}

  """
  def delete_stopover(%Stopover{} = stopover) do
    Repo.delete(stopover)
  end

  @doc """
  Returns a data structure for tracking stopover changes.

  ## Examples

      iex> change_stopover(stopover)
      %Todo{...}

  """
  def change_stopover(%Stopover{} = stopover, attrs \\ %{}) do
    Stopover.changeset(stopover, attrs)
  end
end
