defmodule Postless.Shipments.Shipment do
  @moduledoc """
  Shipment schema and functions.
  """
  use Ecto.Schema

  alias __MODULE__
  alias Postless.Accounts.User
  alias Postless.Repo
  alias Postless.Shipments.ShipmentType
  alias Postless.Stopovers.Stopover
  alias Postless.Stopovers.StopoverType

  import Ecto.Changeset
  import Ecto.Query

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  @type t :: %__MODULE__{
          id: Ecto.UUID.t(),
          address: String.t(),
          comment: String.t() | nil,
          is_delivered: boolean(),
          requested_delivery: NaiveDateTime.t() | nil,
          receiver_id: Ecto.UUID.t() | nil,
          sender_id: Ecto.UUID.t(),
          type_id: Ecto.UUID.t()
        }

  schema "shipments" do
    field :address, :string
    field :comment, :string
    field :is_delivered, :boolean, default: false
    field :requested_delivery, :naive_datetime

    belongs_to(:sender, User,
      references: :id,
      foreign_key: :sender_id,
      type: :binary_id
    )

    belongs_to(:receiver, User,
      references: :id,
      foreign_key: :receiver_id,
      type: :binary_id
    )

    belongs_to(:shipment_type, ShipmentType,
      references: :id,
      foreign_key: :type_id,
      type: :binary_id
    )

    has_many :stopovers, {"stopovers", Stopover}

    timestamps()
  end

  @doc false
  def changeset(shipment, attrs) do
    shipment
    |> cast(attrs, [
      :address,
      :is_delivered,
      :comment,
      :requested_delivery,
      :sender_id,
      :type_id,
      :receiver_id
    ])
    |> validate_required([:address, :is_delivered, :sender_id, :type_id])
  end

  @spec create(
          sender_id :: Ecto.UUID.t(),
          address :: String.t(),
          type_id :: Ecto.UUID.t(),
          receiver_id :: Ecto.UUID.t() | nil,
          comment :: String.t() | nil,
          requested_delivery :: NaiveDateTime.t() | nil,
          sender_address :: String.t()
        ) :: :ok | {:error, any()}
  def create(
        sender_id,
        receiver_address,
        type_id,
        receiver_id,
        comment,
        requested_delivery,
        sender_address
      ) do
    Repo.transaction(fn ->
      {:ok, shipment} =
        %Shipment{}
        |> changeset(%{
          sender_id: sender_id,
          address: receiver_address,
          type_id: type_id,
          receiver_id: receiver_id,
          comment: comment,
          requested_delivery: requested_delivery,
          is_delivered: false
        })
        |> Repo.insert()

      Stopover.create(
        shipment.id,
        :ok,
        StopoverType.get_by_type("start").id,
        sender_address,
        sender_id
      )

      shipment
    end)
  end

  @spec update(shipment :: Shipment.t(), attrs :: map) :: {:ok, Shipment.t()} | {:error, map()}
  def update(shipment, attrs) do
    shipment
    |> changeset(attrs)
    |> Repo.update()
  end

  @spec all() :: [Shipment.t()]
  def all do
    Repo.all(
      from(so in Shipment,
        preload: [:receiver, :sender, :shipment_type],
        order_by: [so.is_delivered, desc: so.inserted_at]
      )
    )
  end

  @spec get(id :: Ecto.UUID.t()) :: Shipment.t()
  def get(id) do
    Repo.get(Shipment, id)
  end

  @spec get_by(params :: map) :: [Shipment.t()]
  def get_by(params) do
    Repo.get_by(Shipment, params)
  end

  @spec get_for_user(user_id :: Ecto.UUID.t(), is_delivered? :: boolean()) :: [Shipment.t()]
  def get_for_user(user_id, is_delivered?) do
    Repo.all(
      from(s in Shipment,
        where: s.sender_id == ^user_id,
        where: s.is_delivered == ^is_delivered?,
        preload: [:receiver, :shipment_type],
        order_by: [desc: s.updated_at]
      )
    )
  end

  @spec delete(shipment :: Shipment.t()) :: {:ok, Shipment.t()} | {:error, map()}
  def delete(shipment) do
    Repo.delete(shipment)
  end

  @spec create_secret(Ecto.UUID.t() | Shipment.t()) :: String.t()
  def create_secret(id) when is_binary(id) do
    create_secret(get(id))
  end

  def create_secret(shipment) do
    :crypto.mac(:hmac, :md5, shipment.sender_id, shipment.id)
    |> Base.encode16()
    |> String.downcase()
  end

  @spec deliver(Ecto.UUID.t() | Shipment.t(), secret :: String.t()) :: :ok | {:error, any()}
  def deliver(shipment_id, secret) when is_binary(shipment_id) do
    deliver(get(shipment_id), secret)
  end

  def deliver(shipment, secret) do
    case create_secret(shipment) do
      ^secret ->
        last_stopover = List.last(Stopover.get_by_shipment_id(shipment.id))

        if is_nil(last_stopover) do
          {:error, :no_stopover}
        else
          case Repo.transaction(fn ->
                 Stopover.update(last_stopover, %{
                   type_id: StopoverType.get_by_type("delivery").id
                 })

                 Shipment.update(shipment, %{is_delivered: true})
               end) do
            {:ok, _any} -> :ok
            _any -> {:error, :db_error}
          end
        end

      _other_secret ->
        {:error, :wrong_secret}
    end
  end

  @spec count_for_user(user_id :: Ecto.UUID.t()) :: integer()
  def count_for_user(user_id) do
    Repo.one(
      from s in Shipment,
        where: s.sender_id == ^user_id,
        select: count(1)
    )
  end
end
