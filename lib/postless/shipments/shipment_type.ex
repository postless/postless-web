defmodule Postless.Shipments.ShipmentType do
  @moduledoc """
  ShipmentType schema and CRUD functions.
  """

  use Ecto.Schema

  alias __MODULE__
  alias Postless.Repo

  import Ecto.Changeset
  import Ecto.Query

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  @type t :: %__MODULE__{
          id: Ecto.UUID.t(),
          points: integer(),
          type: String.t()
        }
  schema "shipment_types" do
    field :points, :integer
    field :type, :string

    timestamps()
  end

  @spec changeset(shipment_type :: map(), attrs :: map()) :: map()
  def changeset(shipment_type, attrs) do
    shipment_type
    |> cast(attrs, [:type, :points])
    |> validate_required([:type, :points])
    |> validate_number(:points, greater_than: 0)
  end

  @spec create(type :: String.t(), points :: Integer.t()) ::
          {:ok, ShipmentType.t()} | {:error, map()}
  def create(type, points) do
    %ShipmentType{}
    |> changeset(%{type: type, points: points})
    |> Repo.insert()
  end

  @spec update_points(Ecto.UUID.t() | String.t(), points :: Integer.t()) ::
          {:ok, Shiptment.t()} | {:error, map()}
  def update_points(id, points) when is_binary(id) do
    id
    |> get()
    |> changeset(%{points: points})
    |> Repo.update()
  end

  def update_points(type, points) do
    type
    |> get_by_type()
    |> changeset(%{points: points})
    |> Repo.update()
  end

  @spec get_all() :: [ShipmentType.t()]
  def get_all do
    Repo.all(from(st in ShipmentType, order_by: st.type))
  end

  @spec get(id :: Ecto.UUID.t()) :: ShipmentType.t()
  def get(id) do
    Repo.get(ShipmentType, id)
  end

  @spec get_by_type(type :: String.t()) :: ShipmentType.t()
  def get_by_type(type) do
    Repo.one(from(st in ShipmentType, where: st.type == ^type))
  end
end
