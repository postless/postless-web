defmodule Postless.AuthenticationEmail do
  import Bamboo.Email

  use Phoenix.VerifiedRoutes, endpoint: PostlessWeb.Endpoint, router: PostlessWeb.Router

  @template """
  Here is your login link:

  <%= login_url %>

  It is valid for 30 minutes.
  """

  @doc """
    The sign in email containing the login link.
  """
  def login_link(token_value, user) do
    login_url = url(~p"/signin/#{token_value}")
    body = EEx.eval_string(@template, login_url: login_url)

    new_email()
    |> to(user.email)
    |> from(System.get_env("EMAIL_SENDER", "login@postless.org"))
    |> subject("Your login link")
    |> text_body(body)
  end
end
