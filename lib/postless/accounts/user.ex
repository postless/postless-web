defmodule Postless.Accounts.User do
  use Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query

  alias Postless.Accounts.AuthToken
  alias Postless.Repo
  alias Postless.Shipments.Shipment
  alias Postless.Shipments.ShipmentType
  alias Postless.Stopovers.Stopover
  alias Postless.Stopovers.StopoverType

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "users" do
    field :email, :string
    field :name, :string
    field :username, :string

    has_many :auth_tokens, AuthToken

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :username, :name])
    |> validate_required([:email])
    |> unique_constraint([:email])
  end

  @spec calc_points(user_id :: Ecto.UUID.t(), String.t()) :: integer()
  def calc_points(user_id, "shipments") do
    Repo.one(
      from(sh in Shipment,
        join: sh_type in ShipmentType,
        on: sh_type.id == sh.type_id,
        where: sh.sender_id == ^user_id,
        select: sum(sh_type.points)
      )
    ) || 0
  end

  def calc_points(user_id, "stopovers") do
    Repo.one(
      from(st in Stopover,
        join: st_type in StopoverType,
        on: st_type.id == st.type_id,
        where: st.user_id == ^user_id,
        select: sum(st_type.points)
      )
    ) || 0
  end

  @spec calc_points(user_id :: Ecto.UUID.t()) :: integer()
  def calc_points(user_id) do
    calc_points(user_id, "shipments") + calc_points(user_id, "stopovers")
  end
end
