defmodule Postless.Accounts.AuthToken do
  use Ecto.Schema
  import Ecto.Changeset

  alias Postless.Accounts.User
  alias PostlessWeb.Endpoint
  alias Phoenix.Token

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "auth_tokens" do
    field :value, :string
    belongs_to :user, User

    timestamps(updated_at: false)
  end

  @doc false
  def changeset(auth_token, user) do
    auth_token
    # convert the struct without taking any params
    |> cast(%{}, [])
    |> put_assoc(:user, user)
    |> put_change(:value, generate_token(user))
    |> validate_required([:value, :user])
    |> unique_constraint(:value)
  end

  # generate a random and url-encoded token of given length
  defp generate_token(nil), do: nil

  defp generate_token(user) do
    Token.sign(Endpoint, "user", user.id)
  end
end
