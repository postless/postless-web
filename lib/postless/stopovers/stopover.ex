defmodule Postless.Stopovers.Stopover do
  @moduledoc """
  Stopover Schema and CRUD functions.
  """
  use Ecto.Schema

  alias __MODULE__
  alias Postless.Accounts.User
  alias Postless.Repo
  alias Postless.Shipments.Shipment
  alias Postless.Stopovers.StopoverType

  import Ecto.Changeset
  import Ecto.Query

  @status [
    :ok,
    :broken,
    :wet,
    :unpackaged,
    :repackaged
  ]

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  @type t :: %__MODULE__{
          id: Ecto.UUID.t(),
          status: Ecto.Enum.t(),
          address: String.t() | nil,
          user_id: Ecto.UUID.t(),
          shipment_id: Ecto.UUID.t(),
          type_id: Ecto.UUID.t()
        }

  schema "stopovers" do
    field :address, :string
    field :status, Ecto.Enum, values: @status

    belongs_to(:user, User,
      references: :id,
      foreign_key: :user_id,
      type: :binary_id
    )

    belongs_to(:shipment, Shipment,
      references: :id,
      foreign_key: :shipment_id,
      type: :binary_id
    )

    belongs_to(:type, StopoverType,
      references: :id,
      foreign_key: :type_id,
      type: :binary_id
    )

    timestamps()
  end

  @doc false
  def changeset(stopover, attrs) do
    stopover
    |> cast(attrs, [:status, :address, :type_id, :user_id])
    |> validate_required([:status, :type_id])
  end

  @spec create(
          shipment_id :: Ecto.UUID.t(),
          status :: Ecto.Enum.t(),
          type_id :: Ecto.UUID.t(),
          address :: String.t(),
          user_id :: Ecto.UUID.t() | nil
        ) :: {:ok, Stopover.t()} | {:error, map()}
  def create(shipment_id, status, type_id, address, user_id \\ nil) do
    %Stopover{shipment_id: shipment_id}
    |> changeset(%{
      status: status,
      type_id: type_id,
      address: address,
      user_id: user_id
    })
    |> Repo.insert()
  end

  @spec update(stopover :: Stopover.t(), attrs :: map) :: {:ok, Stopover.t()} | {:error, map()}
  def update(stopover, attrs) do
    stopover
    |> changeset(attrs)
    |> Repo.update()
  end

  @spec get_all() :: [Stopover.t()]
  def get_all do
    Repo.all(from(so in Stopover, order_by: [desc: so.inserted_at]))
  end

  @spec get(id :: Ecto.UUID.t()) :: Stopover.t()
  def get(id) do
    Stopover
    |> Repo.get(id)
    |> Repo.preload([:user, :shipment, :type])
  end

  @spec delete(stopover :: Stopover.t()) :: {:ok, Stopover.t()} | {:error, map()}
  def delete(stopover) do
    Repo.delete(stopover)
  end

  @spec get_by_shipment_id(shipment_id :: Ecto.UUID.t()) :: [Stopover.t()]
  def get_by_shipment_id(shipment_id) do
    Repo.all(
      from(so in Stopover, where: so.shipment_id == ^shipment_id, order_by: so.inserted_at)
    )
  end

  @spec count_for_user(user_id :: Ecto.UUID.t()) :: integer()
  def count_for_user(user_id) do
    Repo.one(
      from s in Stopover,
        where: s.user_id == ^user_id,
        select: count(1)
    )
  end
end
