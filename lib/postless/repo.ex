defmodule Postless.Repo do
  use Ecto.Repo,
    otp_app: :postless,
    adapter: Ecto.Adapters.Postgres
end
