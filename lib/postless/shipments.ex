defmodule Postless.Shipments do
  @moduledoc """
  The Shipments context.
  """

  import Ecto.Query, warn: false
  alias Postless.Repo

  alias Postless.Shipments.Shipment

  @doc """
  Returns the list of shipments.

  ## Examples

      iex> list_shipments()
      [%Shipment{}, ...]

  """
  def list_shipments do
    Repo.all(Shipment)
  end

  @doc """
  Gets a single shipment.

  Raises if the Shipment does not exist.

  ## Examples

      iex> get_shipment!(123)
      %Shipment{}

  """
  def get_shipment!(id),
    do: Shipment |> Repo.get!(id) |> Repo.preload([:sender, :shipment_type, :stopovers])

  @doc """
  Creates a shipment.

  ## Examples

      iex> create_shipment(%{field: value})
      {:ok, %Shipment{}}

      iex> create_shipment(%{field: bad_value})
      {:error, ...}

  """
  def create_shipment(
        current_user_id,
        address,
        type_id,
        receiver_id,
        comment,
        requested_delivery,
        sender_address
      ) do
    Shipment.create(
      current_user_id,
      address,
      type_id,
      receiver_id,
      comment,
      requested_delivery,
      sender_address
    )
  end

  @doc """
  Updates a shipment.

  ## Examples

      iex> update_shipment(shipment, %{field: new_value})
      {:ok, %Shipment{}}

      iex> update_shipment(shipment, %{field: bad_value})
      {:error, ...}

  """
  def update_shipment(%Shipment{} = shipment, attrs) do
    shipment
    |> Shipment.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Shipment.

  ## Examples

      iex> delete_shipment(shipment)
      {:ok, %Shipment{}}

      iex> delete_shipment(shipment)
      {:error, ...}

  """
  def delete_shipment(%Shipment{} = shipment) do
    Repo.delete(shipment)
  end

  @doc """
  Returns a data structure for tracking shipment changes.

  ## Examples

      iex> change_shipment(shipment)
      %Todo{...}

  """
  def change_shipment(%Shipment{} = shipment, attrs \\ %{}) do
    Shipment.changeset(shipment, attrs)
  end
end
