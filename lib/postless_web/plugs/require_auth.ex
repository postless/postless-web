defmodule PostlessWeb.Plugs.RequireAuth do
  import Plug.Conn
  import Phoenix.Controller

  use Phoenix.VerifiedRoutes,
    router: PostlessWeb.Router,
    endpoint: PostlessWeb.Endpoint,
    statics: ~w(images)

  def init(_params) do
  end

  def call(conn, _params) do
    if conn.assigns.user_signed_in? do
      conn
    else
      conn
      |> put_flash(:error, "You need to sign in or sign up before continuing.")
      |> redirect(to: ~p"/sessions/new")
      |> halt()
    end
  end
end
