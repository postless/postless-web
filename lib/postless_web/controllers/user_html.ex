defmodule PostlessWeb.UserHTML do
  use PostlessWeb, :html

  import PostlessWeb.Gettext

  embed_templates "user_html/*"
end
