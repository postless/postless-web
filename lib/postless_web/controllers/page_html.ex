defmodule PostlessWeb.PageHTML do
  use PostlessWeb, :html

  import PostlessWeb.Gettext

  embed_templates "page_html/*"
end
