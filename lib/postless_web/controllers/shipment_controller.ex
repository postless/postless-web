defmodule PostlessWeb.ShipmentController do
  use PostlessWeb, :controller

  alias Postless.Accounts
  alias Postless.Shipments
  alias Postless.Shipments.Shipment
  alias Postless.Shipments.ShipmentType

  def index(conn, _params) do
    shipments = Shipments.list_shipments()
    render(conn, :index, shipments: shipments)
  end

  def new(conn, _params) do
    changeset = Shipments.change_shipment(%Shipment{})

    render(conn, :new,
      changeset: changeset,
      shipment_types: ShipmentType.get_all(),
      users: Accounts.list_users()
    )
  end

  def create(conn, %{
        "shipment" =>
          %{
            "address" => address,
            "comment" => comment,
            "receiver_id" => receiver_id,
            "requested_delivery" => requested_delivery,
            "type_id" => type_id
          } = _shipment_params
      }) do
    current_user_id = get_session(conn, :user_id)

    receiver_id =
      case receiver_id do
        "" -> nil
        receiver_id -> receiver_id
      end

    requested_delivery =
      case requested_delivery do
        "" -> nil
        requested_delivery -> requested_delivery
      end

    case Shipments.create_shipment(
           current_user_id,
           address,
           type_id,
           receiver_id,
           comment,
           requested_delivery,
           ""
         ) do
      {:ok, shipment} ->
        conn
        |> put_flash(:info, "Shipment created successfully.")
        |> redirect(to: ~p"/shipments/#{shipment}")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :new, changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    shipment = Shipments.get_shipment!(id)
    render(conn, :show, shipment: shipment)
  end

  def edit(conn, %{"id" => id}) do
    shipment = Shipments.get_shipment!(id)
    changeset = Shipments.change_shipment(shipment)

    render(conn, :edit,
      shipment: shipment,
      changeset: changeset,
      shipment_types: ShipmentType.get_all(),
      users: Accounts.list_users()
    )
  end

  def update(conn, %{"id" => id, "shipment" => shipment_params}) do
    shipment = Shipments.get_shipment!(id)

    case Shipments.update_shipment(shipment, shipment_params) do
      {:ok, shipment} ->
        conn
        |> put_flash(:info, "Shipment updated successfully.")
        |> redirect(to: ~p"/shipments/#{shipment}")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :edit,
          shipment: shipment,
          changeset: changeset,
          shipment_types: ShipmentType.get_all(),
          users: Accounts.list_users()
        )
    end
  end

  def delete(conn, %{"id" => id}) do
    shipment = Shipments.get_shipment!(id)
    {:ok, _shipment} = Shipments.delete_shipment(shipment)

    conn
    |> put_flash(:info, "Shipment deleted successfully.")
    |> redirect(to: ~p"/shipments")
  end

  def deliver(conn, %{"id" => shipment_id, "secret" => secret}) do
    changeset = Shipments.change_shipment(Shipments.get_shipment!(shipment_id))

    conn =
      case Shipment.deliver(shipment_id, secret) do
        :ok ->
          conn
          |> clear_flash()
          |> put_flash(:info, "Shipment successfully delivered.")
          |> redirect(to: "/shipments/#{shipment_id}")

        {:error, :wrong_secret} ->
          conn
          |> clear_flash()
          |> put_flash(:error, "Wrong secret, please try again!")

        _error ->
          conn
          |> clear_flash()
          |> put_flash(
            :error,
            "Oops, something went wrong. Please try again or contact our support!"
          )
      end

    render(conn, :deliver, changeset: changeset, shipment_id: shipment_id)
  end

  def deliver(conn, %{"id" => shipment_id}) do
    changeset = Shipments.change_shipment(Shipments.get_shipment!(shipment_id))
    render(conn, :deliver, shipment_id: shipment_id, changeset: changeset)
  end
end
