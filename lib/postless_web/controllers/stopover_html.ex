defmodule PostlessWeb.StopoverHTML do
  use PostlessWeb, :html

  import PostlessWeb.Gettext

  embed_templates "stopover_html/*"

  @doc """
  Renders a stopover form.
  """
  attr :changeset, Ecto.Changeset, required: true
  attr :action, :string, required: true
  attr :shipment_id, Ecto.UUID, required: false

  def stopover_form(assigns)
end
