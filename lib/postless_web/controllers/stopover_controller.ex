defmodule PostlessWeb.StopoverController do
  use PostlessWeb, :controller

  alias Postless.Stopovers
  alias Postless.Stopovers.Stopover
  alias Postless.Stopovers.StopoverType

  def index(conn, _params) do
    stopovers = Stopovers.list_stopovers()
    render(conn, :index, stopovers: stopovers)
  end

  def new(conn, %{"shipment_id" => shipment_id} = _params) do
    changeset = Stopovers.change_stopover(%Stopover{})
    render(conn, :new, changeset: changeset, shipment_id: shipment_id)
  end

  def create(conn, %{
        "stopover" =>
          %{
            "status" => status,
            "shipment_id" => shipment_id,
            "address" => address
          } = _stopover_params
      }) do
    current_user_id = get_session(conn, :user_id)

    address =
      case address do
        "" -> nil
        address -> address
      end

    case Stopover.create(
           shipment_id,
           status,
           StopoverType.get_by_type("ongoing").id,
           address,
           current_user_id
         ) do
      {:ok, stopover} ->
        conn
        |> put_flash(:info, "Stopover created successfully.")
        |> redirect(to: ~p"/stopovers/#{stopover}")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :new, changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    stopover = Stopovers.get_stopover!(id)
    render(conn, :show, stopover: stopover)
  end

  def edit(conn, %{"id" => id}) do
    stopover = Stopovers.get_stopover!(id)
    changeset = Stopovers.change_stopover(stopover)
    render(conn, :edit, stopover: stopover, changeset: changeset)
  end

  def update(conn, %{"id" => id, "stopover" => stopover_params}) do
    stopover = Stopovers.get_stopover!(id)

    case Stopovers.update_stopover(stopover, stopover_params) do
      {:ok, stopover} ->
        conn
        |> put_flash(:info, "Stopover updated successfully.")
        |> redirect(to: ~p"/stopovers/#{stopover}")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :edit, stopover: stopover, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    stopover = Stopovers.get_stopover!(id)
    {:ok, _stopover} = Stopovers.delete_stopover(stopover)

    conn
    |> put_flash(:info, "Stopover deleted successfully.")
    |> redirect(to: ~p"/stopovers")
  end
end
