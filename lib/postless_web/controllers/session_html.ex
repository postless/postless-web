defmodule PostlessWeb.SessionHTML do
  use PostlessWeb, :html

  embed_templates "session_html/*"
end
