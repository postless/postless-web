defmodule PostlessWeb.ShipmentHTML do
  use PostlessWeb, :html

  import PostlessWeb.Gettext

  alias Postless.Shipments
  alias Postless.Stopovers.Stopover

  embed_templates "shipment_html/*"

  @doc """
  Renders a shipment form.
  """
  attr :changeset, Ecto.Changeset, required: true
  attr :action, :string, required: true
  attr :users, Ecto.Enum, required: false
  attr :shipment_types, Ecto.Enum, required: false
  attr :list, :atom, required: false
  slot :inner_block, required: false

  def shipment_form(assigns)

  def get_stopover_type(type_id) do
    Postless.Stopovers.StopoverType.get(type_id)
  end

  def get_secret(_shipment_id, nil) do
    "Not your shipment, not your secret ;)"
  end

  def get_secret(shipment_id, user) do
    if Shipments.get_shipment!(shipment_id).sender_id == user.id do
      Postless.Shipments.Shipment.create_secret(shipment_id)
    else
      "Not your shipment, not your secret ;)"
    end
  end

  def get_stopover_username(stopover_id) do
    stopover = Stopover.get(stopover_id)

    if is_nil(stopover.user) do
      "Added from a guest."
    else
      if !is_nil(stopover.user.username) do
        stopover.user.username
      else
        stopover.user.email
      end
    end
  end
end
