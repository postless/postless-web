defmodule PostlessWeb.UserController do
  use PostlessWeb, :controller

  alias Postless.Accounts
  alias Postless.Accounts.User
  alias Postless.Shipments.Shipment

  alias PostlessWeb.TokenAuthentication

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, :index, users: users)
  end

  def new(conn, _params) do
    # TODO: remove or use
    # @kaputse: why did you add this line?
    # current_user_id = get_session(conn, :user_id)

    if conn.assigns.user_signed_in? do
      conn
      |> put_flash(:error, "You are already signed in.")
      |> redirect(to: ~p"/")
      |> halt()

      conn
    else
      changeset = Accounts.change_user(%User{})
      render(conn, :new, changeset: changeset)
    end
  end

  def create(conn, %{"user" => user_params}) do
    case Accounts.create_user(user_params) do
      {:ok, user} ->
        TokenAuthentication.provide_token(user)

        conn
        |> put_flash(:info, "User created successfully.")
        |> redirect(to: ~p"/")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :new, changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    open_shipments = Shipment.get_for_user(id, false)
    delivered_shipments = Shipment.get_for_user(id, true)

    render(conn, :show,
      user: user,
      open_shipments: open_shipments,
      delivered_shipments: delivered_shipments
    )
  end

  def edit(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    changeset = Accounts.change_user(user)
    render(conn, :edit, user: user, changeset: changeset)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)

    case Accounts.update_user(user, user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "User updated successfully.")
        |> redirect(to: ~p"/users/#{user}")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :edit, user: user, changeset: changeset)
    end
  end

  # def delete(conn, %{"id" => id}) do
  #   user = Accounts.get_user!(id)
  #   {:ok, _user} = Accounts.delete_user(user)

  #   conn
  #   |> put_flash(:info, "User deleted successfully.")
  #   |> redirect(to: ~p"/users")
  # end
end
