<!DOCTYPE html>
<html lang="en" data-theme="lemonade">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content={get_csrf_token()} />
    <%= if System.get_env("MIX_ENV", "prod") == "dev" do %>
      <.live_title>
        <%= assigns[:page_title] || "## Postless dev ##" %>
      </.live_title>
    <% else %>
      <.live_title>
        <%= assigns[:page_title] || "Postless" %>
      </.live_title>
    <% end %>
    <link phx-track-static rel="stylesheet" href={~p"/assets/app.css"} />
    <script defer phx-track-static type="text/javascript" src={~p"/assets/app.js"}>
    </script>
  </head>
  <body class="bg-white antialiased">


      <div class="navbar bg-primary">
        <div class="navbar-start">
          <div class="dropdown">
            <label tabindex="0" class="btn btn-ghost btn-circle">
              <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h7" /></svg>
            </label>
            <ul tabindex="0" class="menu menu-compact dropdown-content mt-3 p-2 shadow bg-base-100 rounded-box w-52">
              <li>
                <.link navigate="/shipments" >
                  <%= gettext("Shipments") %>
                </.link>
              </li>
              <li>
                <.link navigate="/users" >
                  <%= gettext("Users") %>
                </.link>
              </li>
              <li><a><%= gettext("About")%></a></li>
            </ul>
          </div>
        </div>
        <%= if System.get_env("MIX_ENV", "prod") == "dev" do %>
          <div class="navbar-center">
            <a class="btn btn-ghost normal-case text-xl" href="/">## postless dev ##</a>
          </div>
        <% else %>
          <div class="navbar-center">
            <a class="btn btn-ghost normal-case text-xl" href="/">postless</a>
          </div>
        <% end %>
        <div class="navbar-end">
          <ul class="menu menu-horizontal px-1">
            <li tabindex="0">
              <%= if @user_signed_in? do %>
                <.link navigate={"/users/#{@current_user.id}"} >
                    <%= if !is_nil(@current_user.username) do %>
                      <%= @current_user.username %>
                    <% else %>
                      <%= @current_user.email %>
                    <% end %>
                  <svg class="fill-current" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z"/></svg>
                </.link>
                <ul class="p-2 bg-base-100">
                  <li>
                    <.link navigate={"/users/#{@current_user.id}"} >
                      <%= gettext("My Account") %>
                    </.link>
                  </li>
                  <li>
                    <a>
                      <%= gettext("Points") %>: <%= Postless.Accounts.User.calc_points(@current_user.id) %>
                    </a>
                  </li>
                  <li>
                    <.link navigate="/signout" >
                      <%= gettext("Logout") %>
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-5 h-5">
                        <path fill-rule="evenodd" d="M3 4.25A2.25 2.25 0 015.25 2h5.5A2.25 2.25 0 0113 4.25v2a.75.75 0 01-1.5 0v-2a.75.75 0 00-.75-.75h-5.5a.75.75 0 00-.75.75v11.5c0 .414.336.75.75.75h5.5a.75.75 0 00.75-.75v-2a.75.75 0 011.5 0v2A2.25 2.25 0 0110.75 18h-5.5A2.25 2.25 0 013 15.75V4.25z" clip-rule="evenodd" />
                        <path fill-rule="evenodd" d="M19 10a.75.75 0 00-.75-.75H8.704l1.048-.943a.75.75 0 10-1.004-1.114l-2.5 2.25a.75.75 0 000 1.114l2.5 2.25a.75.75 0 101.004-1.114l-1.048-.943h9.546A.75.75 0 0019 10z" clip-rule="evenodd" />
                      </svg>
                    </.link>
                  </li>
                </ul>
              <% else %>
                <.link navigate="/signin" >
                  <%= gettext("Login") %>
                  <svg class="fill-current" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z"/></svg>
                </.link>
                <ul class="p-2 bg-base-100">
                  <li>
                    <.link navigate="/signup" >
                      <%= gettext("Register") %>
                    </.link>
                  </li>
                </ul>
              <% end %>
            </li>
          </ul>
        </div>
      </div>

    <%= @inner_content %>
  </body>
</html>
