defmodule PostlessWeb.Layouts do
  use PostlessWeb, :html

  embed_templates "layouts/*"
end
