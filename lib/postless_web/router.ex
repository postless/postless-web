defmodule PostlessWeb.Router do
  use PostlessWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {PostlessWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug PostlessWeb.Plugs.SetCurrentUser
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :user_authentication do
    plug PostlessWeb.Plugs.RequireAuth
  end

  scope "/", PostlessWeb do
    pipe_through [:browser, :user_authentication]

    resources "/shipments", ShipmentController, only: [:create, :new, :edit, :update]
    resources "/users", UserController, only: [:show, :edit]
  end

  scope "/", PostlessWeb do
    pipe_through :browser

    get "/", PageController, :home

    get "/signin", SessionController, :new
    get "/signin/:token", SessionController, :show, as: :signin
    resources "/sessions", SessionController, only: [:new, :create, :delete]
    # TODO remove when the logout button is done
    get "/signout", SessionController, :delete

    get "/signup", UserController, :new
    resources "/users", UserController, only: [:create, :index, :update]
    resources "/shipments", ShipmentController, only: [:show, :index]
    get "/shipments/:id/deliver", ShipmentController, :deliver
    post "/shipments/:id/deliver", ShipmentController, :deliver
    get "/shipments/:id/deliver/:secret", ShipmentController, :deliver
    resources "/stopovers", StopoverController, only: [:new, :create, :show, :edit]
  end

  # Other scopes may use custom stacks.
  # scope "/api", PostlessWeb do
  #   pipe_through :api
  # end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:postless, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: PostlessWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
