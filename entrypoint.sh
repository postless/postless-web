#!/bin/bash

str=`date -Ins | md5sum`
name=${str:0:10}

# mix phx.digest
# mix ecto.create
mix ecto.migrate
mix phil_columns.seed
# mix run priv/repo/seeds.exs

echo "Starting postless as $name"

elixir --sname $name --cookie monster -S mix phx.server
